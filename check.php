#!/usr/bin/env php
<?php
/**
 *
 * Mattermost PHP Driver https://github.com/ThibaudDauce/mattermost-php
 * Guzzle doc: http://docs.guzzlephp.org/en/stable/quickstart.html
 * 
 */
require __DIR__ . '/vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response ;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\ConnectException ;
use ThibaudDauce\Mattermost\Mattermost;
use ThibaudDauce\Mattermost\Message;
use ThibaudDauce\Mattermost\Attachment;

for( ; true ; )
{
	$config = loadConfig( __DIR__.'/config.json' );

	check( $config );

	sleep( $config->check->sleep );
}

/**
 * 
 * @param object $config
 */
function check( $config )
{
	$promises = [] ;
	
	$httpClient = new GuzzleHttp\Client();
	$validStatusCodes = [200];
	
	/*
	 * Launch http requests
	 */
	foreach( $config->targets as $target )
	{
		// Send an asynchronous request.
		$request = new \GuzzleHttp\Psr7\Request('GET', $target->url, [
			'User-Agent' => 'HttpALiveToMattermost-check',
		]);
		$promises[ $target->label ] = $httpClient->sendAsync($request, [
			'connect_timeout' => $target->timeout,
			'read_timeout' => $target->timeout,
			'timeout' => $target->timeout,
		]);
	}
	// Wait for the requests to complete, even if some of them fail
	$results = Promise\settle($promises)->wait();
	
	/*
	 * Process requests results
	 */
	$hasErrors = false ;
	foreach( $results as $label => $result )
	{
		//echo $label, "\n" ;
		switch( $result['state'] )
		{
		case 'fulfilled':
			// Ok
			//echo "\t", 'Ok', "\n";
			break;
		case 'rejected':
		default :
			// An error occured
			//echo "\t", join( ',', array_keys($result)),"\n";
			//echo "\t", get_class($result['reason']), "\n";
			$hasErrors = true ;
			/**
			 * @var ConnectException $ex
			 */
			$ex = $result['reason'] ;
			echo $label, ' ERROR ', $ex->getMessage(), "\n";
			sendMessage( $config, '**WARNING** Http alive for **'.$label.'**: '."\n".$ex->getMessage() );
			break;
		}
	}
	
	/*if( ! $hasErrors )
	{
		sendMessage( $config, 'All hosts are ok.' );
	}*/
}

/**
 * 
 * @param object $config
 * @param string $msg
 */
function sendMessage( $config, $msg )
{
	$mattermost = new Mattermost(new Client);
	
	$message = (new Message)
	->text( $msg )
	//->channel( $config->mattermost->channel )
	//->username('A Tester')
	//->iconUrl('https://upload.wikimedia.org/wikipedia/fr/f/f6/Phpunit-logo.gif')
	/*
	->attachment(function(Attachment $attachment) {
		$attachment->fallback('This is the fallback test for the attachment.')
		->success()
		->pretext('This is optional pretext that shows above the attachment.')
		->text('This is the text. **Finaly!**')
		->authorName('Mattermost')
		->authorIcon('http://www.mattermost.org/wp-content/uploads/2016/04/icon_WS.png')
		->authorLink('http://www.mattermost.org/')
		->title('Example attachment', 'http://docs.mattermost.com/developer/message-attachments.html')
		->field('Long field', 'Testing with a very long piece of text that will take up the whole width of the table. And then some more text to make it extra long.', false)
		->field('Column one', 'Testing.', true)
		->field('Column two', 'Testing.', true)
		->field('Column one again', 'Testing.', true)
		->imageUrl('http://www.mattermost.org/wp-content/uploads/2016/03/logoHorizontal_WS.png');
	})
	*/
	;

	$mattermost->send($message, $config->mattermost->webhook);

}

function loadConfig( $filename )
{
	$config = json_decode( file_get_contents($filename));
	return $config ;
}
